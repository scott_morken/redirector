'use strict';

const contexts = require('./lib/context');
const response = require('./lib/response.express');
const Event = require('./lib/event.express');
const Bucket = require('./lib/redirects/bucket');
const s3config = require('./lib/s3config');
const Factory = require('./lib/factory');

const envContext = contexts.getEnvironmentContext(process.env || {});
const bucketConfig = s3config(envContext);
const redirectProvider = new Bucket(bucketConfig, envContext.get('bucketName'), envContext.get('bucketKey'));
const port = envContext.get('expressPort');

const express = require('express');
const app = express();
app.enable('trust proxy');

app.get('/status', function (req, res) {
    return res.status(200).send('Ok');
});

app.get('/*', function (req, res) {
    const handlers = {
        event: new Event(req),
        context: contexts,
        response: response
    }
    const factory = new Factory(handlers, redirectProvider);
    return factory.get(res);
});

app.listen(port, () => {
});
