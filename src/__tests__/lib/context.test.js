const contexts = require('../../lib/context');

test('redirect context empty data generates defaults', () => {
    expect(contexts.getRedirectContext('example.com', '', {}).get('uri')).toBe(null);
    expect(contexts.getRedirectContext('example.com', '', {}).get('statusCode')).toBe(301);
    expect(contexts.getRedirectContext('example.com', '', {}).get('statusDescription')).toBe("Moved Permanently");
});

test('redirect context missing host generates defaults', () => {
    const data = {
        'example.edu': {
            default: {
                uri: 'new.example.edu',
                statusCode: 302,
                statusDescription: 'Found'
            }
        }
    }
    expect(contexts.getRedirectContext('example.com', '/', data).get('uri')).toBe(null);
    expect(contexts.getRedirectContext('example.com', '/', data).get('statusCode')).toBe(301);
    expect(contexts.getRedirectContext('example.com', '/', data).get('statusDescription')).toBe("Moved Permanently");
});

test('redirect context matching host sets values', () => {
    const data = {
        'example.com': {
            default: {
                uri: 'new.example.com',
                statusCode: 302,
                statusDescription: 'Found'
            }
        }
    }
    expect(contexts.getRedirectContext('example.com', '/', data).get('uri')).toBe('new.example.com');
    expect(contexts.getRedirectContext('example.com', '/', data).get('statusCode')).toBe(302);
    expect(contexts.getRedirectContext('example.com', '/', data).get('statusDescription')).toBe('Found');
});

test('redirect context matching host and uri sets values', () => {
    const data = {
        'example.com': {
            "/foo": {
                uri: 'foo.example.com',
                statusCode: 302,
                statusDescription: 'Found'
            }
        }
    }
    expect(contexts.getRedirectContext('example.com', '/foo', data).get('uri')).toBe('foo.example.com');
    expect(contexts.getRedirectContext('example.com', '/foo', data).get('statusCode')).toBe(302);
    expect(contexts.getRedirectContext('example.com', '/foo', data).get('statusDescription')).toBe('Found');
});

test('redirect context matching host and uri not matching sets values from default', () => {
    const data = {
        'example.com': {
            default: {
                uri: 'new.example.com',
                statusCode: 302,
                statusDescription: 'Found'
            },
            "/bar": {
                uri: 'foo.example.com',
                statusCode: 302,
                statusDescription: 'Found'
            }
        }
    }
    expect(contexts.getRedirectContext('example.com', '/foo', data).get('uri')).toBe('new.example.com');
    expect(contexts.getRedirectContext('example.com', '/foo', data).get('statusCode')).toBe(302);
    expect(contexts.getRedirectContext('example.com', '/foo', data).get('statusDescription')).toBe('Found');
});

test('env context empty data generates nulls and defaults', () => {
    expect(contexts.getEnvironmentContext({}).get('bucketRegion')).toBe('us-west-2');
    expect(contexts.getEnvironmentContext({}).get('bucketName')).toBe(null);
    expect(contexts.getEnvironmentContext({}).get('bucketKey')).toBe(null);
});

test('env context sets values', () => {
    const data = {
        BUCKET_REGION: 'us-east-1',
        BUCKET_NAME: 'redirects.bucket.com',
        BUCKET_KEY: '/data/redirects.json'
    }
    expect(contexts.getEnvironmentContext(data).get('bucketRegion')).toBe('us-east-1');
    expect(contexts.getEnvironmentContext(data).get('bucketName')).toBe('redirects.bucket.com');
    expect(contexts.getEnvironmentContext(data).get('bucketKey')).toBe('/data/redirects.json');
});