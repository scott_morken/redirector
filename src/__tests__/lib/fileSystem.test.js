'use strict';

const path = require('path');
const FileSystem = require('../../lib/redirects/fileSystem');

const fileSystem = new FileSystem(path.join(__dirname, 'redirects.json'));

test('fileSystem can retrieve json from file', async () => {
    expect(await fileSystem.fetch()).toEqual({
        "example.org": {
            "default": {
                "uri": "https://foo.org/bar",
                "statusCode": 301,
                "statusDescription": "Moved Permanently"
            },
            "/fiz/buz": {
                "uri": "https://buz.org/fiz",
                "statusCode": 301,
                "statusDescription": "Moved Permanently"
            }
        }
    });
});