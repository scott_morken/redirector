'use strict';

const contexts = require('../../lib/context');
const response = require('../../lib/response.v2');
const path = require('path');
const Event = require('../../lib/event.v1');
const Factory = require('../../lib/factory');
const RedirectProvider = require('../../lib/redirects/redirectProvider');
const FileSystem = require('../../lib/redirects/fileSystem');

class RedirectTest extends RedirectProvider {

    constructor() {
        super();
        this.load_data = {
            "example.org": {
                "default": {
                    "uri": "https://foo.org/bar",
                    "statusCode": 301,
                    "statusDescription": "Moved Permanently"
                },
                "/fiz/buz": {
                    "uri": "https://buz.org/fiz",
                    "statusCode": 301,
                    "statusDescription": "Moved Permanently"
                }
            }
        }
    }

    _fetch() {
        this.data = this.load_data;
        return this.data;
    }

}

let event = {
    "Records": [
        {
            "cf": {
                "config": {
                    "distributionDomainName": "d111111abcdef8.cloudfront.net",
                    "distributionId": "EDFDVBD6EXAMPLE",
                    "eventType": "origin-request",
                    "requestId": "4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ=="
                },
                "request": {
                    "clientIp": "203.0.113.178",
                    "headers": {
                        "x-forwarded-for": [
                            {
                                "key": "X-Forwarded-For",
                                "value": "203.0.113.178"
                            }
                        ],
                        "user-agent": [
                            {
                                "key": "User-Agent",
                                "value": "Amazon CloudFront"
                            }
                        ],
                        "via": [
                            {
                                "key": "Via",
                                "value": "2.0 2afae0d44e2540f472c0635ab62c232b.cloudfront.net (CloudFront)"
                            }
                        ],
                        "host": [
                            {
                                "key": "Host",
                                "value": "example.org"
                            }
                        ],
                        "cache-control": [
                            {
                                "key": "Cache-Control",
                                "value": "no-cache, cf-no-cache"
                            }
                        ]
                    },
                    "method": "GET",
                    "origin": {
                        "custom": {
                            "customHeaders": {},
                            "domainName": "example.org",
                            "keepaliveTimeout": 5,
                            "path": "",
                            "port": 443,
                            "protocol": "https",
                            "readTimeout": 30,
                            "sslProtocols": [
                                "TLSv1",
                                "TLSv1.1",
                                "TLSv1.2"
                            ]
                        }
                    },
                    "querystring": "",
                    "uri": "/"
                }
            }
        }
    ]
};

test('factory can return redirect response', async () => {
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new RedirectTest();
    const factory = new Factory(handlers, redirectProvider);
    expect(await factory.get()).toEqual({
        headers: {
            'Location': "https://foo.org/bar",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 301,
        body: "\"Moved Permanently\""
    });
});

test('factory with uri can return redirect response', async () => {
    event.Records[0].cf.request.headers.host[0].value = 'example.org';
    event.Records[0].cf.request.uri = '/fiz/buz';
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new RedirectTest();
    const factory = new Factory(handlers, redirectProvider);
    expect(await factory.get()).toEqual({
        headers: {
            'Location': "https://buz.org/fiz",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 301,
        body: "\"Moved Permanently\""
    });
});

test('factory from file can return redirect response', async () => {
    event.Records[0].cf.request.headers.host[0].value = 'example.org';
    event.Records[0].cf.request.uri = '/';
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new FileSystem(path.join(__dirname, 'redirects.json'));
    const factory = new Factory(handlers, redirectProvider);
    expect(await factory.get()).toEqual({
        headers: {
            'Location': "https://foo.org/bar",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 301,
        body: "\"Moved Permanently\""
    });
});

test('factory from file with uri can return redirect response', async () => {
    event.Records[0].cf.request.headers.host[0].value = 'example.org';
    event.Records[0].cf.request.uri = '/fiz/buz';
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new FileSystem(path.join(__dirname, 'redirects.json'));
    const factory = new Factory(handlers, redirectProvider);
    expect(await factory.get()).toEqual({
        headers: {
            'Location': "https://buz.org/fiz",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 301,
        body: "\"Moved Permanently\""
    });
});

test('factory from file with missing uri can return default redirect response', async () => {
    event.Records[0].cf.request.headers.host[0].value = 'example.org';
    event.Records[0].cf.request.uri = '/buz';
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new FileSystem(path.join(__dirname, 'redirects.json'));
    const factory = new Factory(handlers, redirectProvider);
    expect(await factory.get()).toEqual({
        headers: {
            'Location': "https://foo.org/bar",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 301,
        body: "\"Moved Permanently\""
    });
});

test('factory can return 404 response', async () => {
    event.Records[0].cf.request.headers.host[0].value = 'bar.com';
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new RedirectTest();
    const factory = new Factory(handlers, redirectProvider);
    expect(await factory.get()).toEqual({
        statusCode: 404,
        body: "\"Not found.\""
    });
});

test('factory from file can return 404 response', async () => {
    event.Records[0].cf.request.headers.host[0].value = 'bar.com';
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new FileSystem(path.join(__dirname, 'redirects.json'));
    const factory = new Factory(handlers, redirectProvider);
    expect(await factory.get()).toEqual({
        statusCode: 404,
        body: "\"Not found.\""
    });
});
