const Event = require('../../lib/event.v1');

const event = {
    "Records": [
        {
            "cf": {
                "config": {
                    "distributionDomainName": "d111111abcdef8.cloudfront.net",
                    "distributionId": "EDFDVBD6EXAMPLE",
                    "eventType": "origin-request",
                    "requestId": "4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ=="
                },
                "request": {
                    "clientIp": "203.0.113.178",
                    "headers": {
                        "x-forwarded-for": [
                            {
                                "key": "X-Forwarded-For",
                                "value": "203.0.113.178"
                            }
                        ],
                        "user-agent": [
                            {
                                "key": "User-Agent",
                                "value": "Amazon CloudFront"
                            }
                        ],
                        "via": [
                            {
                                "key": "Via",
                                "value": "2.0 2afae0d44e2540f472c0635ab62c232b.cloudfront.net (CloudFront)"
                            }
                        ],
                        "host": [
                            {
                                "key": "Host",
                                "value": "example.org"
                            }
                        ],
                        "cache-control": [
                            {
                                "key": "Cache-Control",
                                "value": "no-cache, cf-no-cache"
                            }
                        ]
                    },
                    "method": "GET",
                    "origin": {
                        "custom": {
                            "customHeaders": {},
                            "domainName": "example.org",
                            "keepaliveTimeout": 5,
                            "path": "",
                            "port": 443,
                            "protocol": "https",
                            "readTimeout": 30,
                            "sslProtocols": [
                                "TLSv1",
                                "TLSv1.1",
                                "TLSv1.2"
                            ]
                        }
                    },
                    "querystring": "",
                    "uri": "/"
                }
            }
        }
    ]
};

test('can retrieve host header from request event', () => {
    const ev = new Event(event);
    expect(ev.getRequestHeader('Host')).toBe('example.org');
});

test('can retrieve host from request event', () => {
    const ev = new Event(event);
    expect(ev.getHost()).toBe('example.org');
});

test('can retrieve slash uri from request event', () => {
    const ev = new Event(event);
    expect(ev.getUri()).toBe('/');
});

test('can retrieve uri without trailing slash from request event', () => {
    event.Records[0].cf.request.uri = '/foo/bar';
    const ev = new Event(event);
    expect(ev.getUri()).toBe('/foo/bar');
});

test('can retrieve uri with trailing slash from request event', () => {
    event.Records[0].cf.request.uri = '/fiz/buz/';
    const ev = new Event(event);
    expect(ev.getUri()).toBe('/fiz/buz');
});