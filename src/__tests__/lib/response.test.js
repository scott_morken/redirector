const response = require('../../lib/response.v2');

test('no uri is 404', () => {
    expect(response.get()).toEqual({
        body: "\"Not found.\"",
        statusCode: 404,
    });
});

test('uri and default status code and default status description', () => {
    expect(response.get('https://example.org')).toEqual({
        headers: {
            'Location': "https://example.org",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 301,
        body: "\"Moved Permanently\""
    });
});

test('uri and status code and default status description', () => {
    expect(response.get('https://example.org', 302)).toEqual({
        headers: {
            'Location': "https://example.org",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 302,
        body: "\"Moved Permanently\""
    });
});

test('uri and status code and status description', () => {
    expect(response.get('https://example.org', 302, 'Moved Temporarily')).toEqual({
        headers: {
            'Location': "https://example.org",
            'Cache-Control': "max-age=3600"
        },
        isBase64Encoded: false,
        statusCode: 302,
        body: "\"Moved Temporarily\""
    });
})