'use strict';

const contexts = require('./lib/context');
const response = require('./lib/response.v2');
const Event = require('./lib/event.v2');
const Bucket = require('./lib/redirects/bucket');
const Factory = require('./lib/factory');

module.exports.handler = (event, context) => {
    const envContext = contexts.getEnvironmentContext(process.env || {});
    const handlers = {
        event: new Event(event),
        context: contexts,
        response: response
    }
    const redirectProvider = new Bucket(envContext.get('bucketRegion'), envContext.get('bucketName'), envContext.get('bucketKey'), envContext.get('bucketEndpoint'));
    const factory = new Factory(handlers, redirectProvider);
    return factory.get();
}
