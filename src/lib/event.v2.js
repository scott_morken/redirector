'use strict';

class EventV2 {

    constructor(event) {
        this.event = event;
    }

    getRequest() {
        return this.event || {};
    }

    getResponse() {
        return {};
    }

    getRequestHeaders() {
        return this.getRequest().headers || {};
    }

    getRequestHeader(header) {
        return this.getRequestHeaders()[header] || null;
    }

    getHost() {
        return this.getRequestHeader('Host');
    }

    getUri() {
        let uri = this.getRequest().rawPath || '';
        return uri.endsWith('/') && uri.length > 1 ? uri.slice(0, -1) : uri;
    }
}

module.exports = EventV2;