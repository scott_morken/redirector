'use strict';

class EventExpress {

    constructor(event) {
        this.event = event;
    }

    getRequest() {
        return this.event || {};
    }

    getRequestHeaders() {
        return this.getRequest().headers || {};
    }

    getRequestHeader(header) {
        header = header.toLowerCase();
        return this.getRequestHeaders()[header] || null;
    }

    getHost() {
        return this.getRequest().hostname;
    }

    getUri() {
        let uri = this.getRequest().path || '';
        return uri.endsWith('/') && uri.length > 1 ? uri.slice(0, -1) : uri;
    }
}

module.exports = EventExpress;