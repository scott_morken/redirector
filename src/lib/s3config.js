'use strict';

module.exports = function (env) {
    let config = {region: env.get('bucketRegion'), s3ForcePathStyle: true};
    if (env.get('accessKeyId')) {
        config['accessKeyId'] = env.get('accessKeyId');
        config['secretAccessKey'] = env.get('secretAccessKey');
    }
    if (env.get('bucketEndpoint')) {
        config['endpoint'] = env.get('bucketEndpoint');
    }
    return config;
};