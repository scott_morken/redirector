'use strict';

const RedirectProvider = require('./redirectProvider');
const fs = require('fs');

class FileSystem extends RedirectProvider {

    constructor(filename) {
        super();
        this.filename = filename;
    }

    readFile(filename) {
        if (!fs.existsSync(filename)) {
            console.error(filename + ' does not exist.');
            return '';
        }
        return fs.readFileSync(filename, 'utf8');
    }

    async _fetch() {
        const txt = this.readFile(this.filename);
        let data;
        try {
            data = JSON.parse(txt);
        } catch (e) {
            console.error('fetch parse failed', txt, e);
            data = {};
        }
        return data;
    }
}

module.exports = FileSystem;