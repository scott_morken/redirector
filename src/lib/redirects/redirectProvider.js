class RedirectProvider {
    constructor() {
        this.cached = 0;
        this.data = {};
        this.cacheTime = 300;
        if (!this._fetch) {
            throw new Error("Missing _fetch method.");
        }
    }

    async fetch() {
        if (this.dataIsValid() && this.cacheIsValid()) {
            return this.data;
        }
        this.data = await this._fetch();
        this.cached = Date.now();
        return this.data;
    }

    dataIsValid() {
        return Object.keys(this.data).length > 0;
    }

    cacheIsValid() {
        const seconds = Math.floor((Date.now() - this.cached) / 1000);
        const valid = seconds < this.cacheTime;
        if (!valid) {
            this.cached = 0;
            this.data = {};
        }
        return valid;
    }
}

module.exports = RedirectProvider;