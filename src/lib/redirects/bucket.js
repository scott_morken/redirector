'use strict';

const RedirectProvider = require('./redirectProvider');
const AWS = require('aws-sdk');

class Bucket extends RedirectProvider {

    constructor(config, bucket, object) {
        super();
        this.bucket = bucket;
        this.object = object;
        this.config = config;
        this.s3 = new AWS.S3(config);
    }

    async readFile(bucketName, filename) {
        const params = {Bucket: bucketName, Key: filename};
        try {
            const response = await this.s3
                .getObject(params)
                .promise();
            return response.Body.toString();
        } catch (err) {
            console.error('s3.getObject error: [' + this.config['endpoint'] + '][' + this.config['region'] + '][' + bucketName + '][' + filename + '] ' + err);
            return "{}";
        }
    }

    async _fetch() {
        const txt = await this.readFile(this.bucket, this.object);
        let data;
        try {
            data = JSON.parse(txt);
        } catch (e) {
            console.error('fetch parse failed', txt, e);
            data = {};
        }
        return data;
    }
}

module.exports = Bucket;