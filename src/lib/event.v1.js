'use strict';

class EventV1 {

    constructor(event) {
        this.event = event;
    }

    getCF() {
        return this.event.Records[0].cf || {};
    }

    getRequest() {
        return this.getCF().request || {};
    }

    getResponse() {
        return this.getCF().response || {};
    }

    getRequestHeaders() {
        return this.getRequest().headers || {};
    }

    getRequestHeader(header) {
        header = header.toLowerCase();
        return this.getRequestHeaders()[header][0].value || null;
    }

    getHost() {
        return this.getRequestHeader('Host');
    }

    getUri() {
        let uri = this.getRequest().uri || '';
        return uri.endsWith('/') && uri.length > 1 ? uri.slice(0, -1) : uri;
    }
}

module.exports = EventV1;