'use strict';

class Factory {

    constructor(handlers, redirectProvider) {
        this.handlers = handlers;
        this.redirectProvider = redirectProvider;
    }

    async get(response) {
        const host = this.handler('event').getHost();
        const uri = this.handler('event').getUri();
        const redirectsData = await this.redirectProvider.fetch();
        const redirectContext = this.handler('context').getRedirectContext(host, uri, redirectsData);
        return this.handler('response').get(redirectContext.get('uri'), redirectContext.get('statusCode'), redirectContext.get('statusDescription'), response);
    }

    handler(name) {
        return this.handlers[name];
    }
}

module.exports = Factory;