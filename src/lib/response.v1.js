'use strict';

module.exports.get = function (uri, statusCode, statusDescription) {
    if (uri) {
        return {
            isBase64Encoded: false,
            statusCode: statusCode ? statusCode : 301,
            headers: {
                'Location': uri,
                'Cache-Control': "max-age=3600"
            },
            body: JSON.stringify(statusDescription ? statusDescription : 'Moved Permanently')
        }
    }
    return {
        isBase64Encoded: false,
        statusCode: 404,
        body: JSON.stringify("Not found.")
    }
}