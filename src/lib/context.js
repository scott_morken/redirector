'use strict';

function getHostData(host, data) {
    return data[host] || {};
}

function getUriData(uri, hostData) {
    if (Object.prototype.hasOwnProperty.call(hostData, uri)) {
        return hostData[uri] || {};
    }
    return hostData['default'] || {};
}

module.exports.getRedirectContext = function (host, uri, data) {
    let context = getUriData(uri, getHostData(host, data));
    return new Map(Object.entries({
        uri: context.uri || null,
        statusCode: context.statusCode || 301,
        statusDescription: context.statusDescription || 'Moved Permanently'
    }));
}

module.exports.getEnvironmentContext = function (env) {
    return new Map(Object.entries({
        expressPort: env.EXPRESS_PORT || 8000,
        bucketRegion: env.BUCKET_REGION || 'us-west-2',
        bucketName: env.BUCKET_NAME || null,
        bucketKey: env.BUCKET_KEY || null,
        bucketEndpoint: env.BUCKET_ENDPOINT || null,
        accessKeyId: env.ACCESS_KEY_ID || null,
        secretAccessKey: env.SECRET_ACCESS_KEY || null
    }));
}