'use strict';

module.exports.get = function (uri, statusCode, statusDescription, response) {
    if (uri) {
        statusCode = statusCode ? statusCode : 301;
        return response.redirect(statusCode, uri);
    }
    return response.status(404).send('Not found.');
}